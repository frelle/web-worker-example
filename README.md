# Web worker example

How to use:

1. Install dependencies.

```bash
npm i
```

2. Edit the script in index.html to call either 
  - `doHere`, which does work on the main thread, blocking the UI, or 
  - `doThere`, which does work on a background thread, not blocking the UI.

3. Run the application (available on `localhost:8000`).

```bash
npm run dev
```

While the application is running, try editing the text input field to verify whether the UI is blocked.